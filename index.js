// insertOne
db.users.insertOne({
    name: "single",
    accommodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
});

// insertMany

db.users.insertMany([{
    name: "double",
    accommodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false,
}, {
    name: "queen",
    accommodates: 4,
    price:4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false,
}]);

// find to search a room with the name double

db.users.find({name: "double"});

// updateOne method to update queen room and available rooms to 0

db.users.updateOne({name: "queen"}, {$set: {rooms_available: 0}});

// deletemany to delete all rooms that have 0 availability

db.users.deleteMany({rooms_available:0});